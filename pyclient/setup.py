# Copyright 2018 KBA Students Project
# ------------------------------------------------------------------------------
'''
Python package setup (used by Dockerfile).
'''

from setuptools import setup, find_packages

setup(
    name='grocerystore-cli',
    version='1.0',
    description='Automated Grocery Store',
    author='Atul Abhishek',
    url='https://gitlab.com/atulabhishek/sawtooth-grocerystore.git',
    packages=find_packages(),
    install_requires=[
        'aiohttp',
        'colorlog',
        'protobuf',
        'sawtooth-sdk',
        'sawtooth-signing',
        'PyYAML',
    ],
    data_files=[],
    entry_points={
        'console_scripts': [
            'grocerystore = grocerystore_cli:main',
        ]
    })

