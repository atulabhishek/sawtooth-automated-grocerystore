#!/usr/bin/env python3

# Copyright 2018 KBA Students Project
# ------------------------------------------------------------------------------
'''
Command line interface for grocerystore TF.
Parses command line arguments and passes to the GroceryStoreClient class
to process.
'''

import argparse
import logging
import os
import sys
import traceback

from colorlog import ColoredFormatter
from grocerystore_client import GroceryStoreClient

KEY_NAME = 'grocerystore'

DEFAULT_URL = 'http://rest-api:8008'

def create_console_handler(verbose_level):
    '''Setup console logging.'''
    del verbose_level # unused
    clog = logging.StreamHandler()
    formatter = ColoredFormatter(
        "%(log_color)s[%(asctime)s %(levelname)-8s%(module)s]%(reset)s "
        "%(white)s%(message)s",
        datefmt="%H:%M:%S",
        reset=True,
        log_colors={
            'DEBUG': 'cyan',
            'INFO': 'green',
            'WARNING': 'yellow',
            'ERROR': 'red',
            'CRITICAL': 'red',
        })

    clog.setFormatter(formatter)
    clog.setLevel(logging.DEBUG)
    return clog

def setup_loggers(verbose_level):
    '''Setup logging.'''
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logger.addHandler(create_console_handler(verbose_level))

def create_parser(prog_name):
    '''Create the command line argument parser for the grocerystore CLI.'''
    parent_parser = argparse.ArgumentParser(prog=prog_name, add_help=False)

    parser = argparse.ArgumentParser(
        description='Provides subcommands to manage your grocery store',
        parents=[parent_parser])

    parser.add_argument(dest='name',type=str,help='name of the product')

    subparsers = parser.add_subparsers(title='subcommands', dest='command')
    subparsers.required = True

    procure_subparser = subparsers.add_parser('procure',
                                           help='adds product to the grocery store',
                                           parents=[parent_parser])
    procure_subparser.add_argument('amount',
                                type=int,
                                help='the amount of product to add to inventory')
    sell_subparser = subparsers.add_parser('sell',
                                          help='deletes product from the grocery store',
                                          parents=[parent_parser])
    sell_subparser.add_argument('amount',
                               type=int,
                               help='the amount of product to delete from the inventory')
    subparsers.add_parser('count',
                          help='show number of count of product in ' +
                          'the grocery store',
                          parents=[parent_parser])
						  
    clear_subparser = subparsers.add_parser('clear',
                                           help='sets the quantity of product to zero in the inventory',
                                           parents=[parent_parser])					  
						  
    return parser


def _get_private_keyfile(key_name):
    '''Get the private key for key_name.'''
    home = os.path.expanduser("~")
    key_dir = os.path.join(home, ".sawtooth", "keys")
    return '{}/{}.priv'.format(key_dir, key_name)

def do_procure(args):
    '''Subcommand to procure product.  Calls client class for procuring the product.'''
    privkeyfile = _get_private_keyfile(KEY_NAME)
    productname = args.name.lower()
    client = GroceryStoreClient(base_url=DEFAULT_URL, key_file=privkeyfile, product_name=productname)
    response = client.procure(productname, args.amount)
    print("Procure Response: {}".format(response))

def do_sell(args):
    '''Subcommand to sell product.  Calls client class to do the selling.'''
    privkeyfile = _get_private_keyfile(KEY_NAME)
    productname = args.name.lower()
    client = GroceryStoreClient(base_url=DEFAULT_URL, key_file=privkeyfile, product_name=productname)
    response = client.sell(productname, args.amount)
    print("Sell Response: {}".format(response))

def do_count(args):
    '''Subcommand to count the queried product.  Calls client class to do the counting.'''
    privkeyfile = _get_private_keyfile(KEY_NAME)
    productname = args.name.lower()
    client = GroceryStoreClient(base_url=DEFAULT_URL, key_file=privkeyfile, product_name=productname)
    data = client.count(productname)
    if data is not None:
        print("\nThe grocery store has {} product.\n".format(data.decode()))
    else:
        raise Exception("Grocery store data not found")
		
def do_clear(args):
    '''Subcommand to set the amount of product to zero. Calls client class to do the clearing.'''
    privkeyfile = _get_private_keyfile(KEY_NAME)
    productname = args.name.lower()
    client = GroceryStoreClient(base_url=DEFAULT_URL, key_file=privkeyfile, product_name=productname)
    response = client.clear(productname)
    print("Clear Response: {}".format(response))

def main(prog_name=os.path.basename(sys.argv[0]), args=None):
    '''Entry point function for the client CLI.'''
    try:
        if args is None:
            args = sys.argv[1:]
        parser = create_parser(prog_name)
        args = parser.parse_args(args)
        verbose_level = 0
        setup_loggers(verbose_level=verbose_level)
        
        # Get the commands from cli args and call corresponding handlers
        if args.command == 'procure':
            do_procure(args)
        elif args.command == 'sell':
            do_sell(args)
        elif args.command == 'count':
            do_count(args)
        elif args.command == 'clear':
            do_clear(args)	
        else:
            raise Exception("Invalid command: {}".format(args.command))

    except KeyboardInterrupt:
        pass
    except SystemExit as err:
        raise err
    except BaseException as err:
        traceback.print_exc(file=sys.stderr)
        sys.exit(1)

if __name__ == '__main__':
    main()