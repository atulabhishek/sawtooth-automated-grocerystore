# Copyright 2018 KBA Students Project
# ------------------------------------------------------------------------------
'''
GroceryStoreTransactionHandler class interfaces for grocerystore Transaction Family.
'''

import hashlib
import logging

from sawtooth_sdk.processor.handler import TransactionHandler
from sawtooth_sdk.processor.exceptions import InvalidTransaction
from sawtooth_sdk.processor.exceptions import InternalError
from sawtooth_sdk.processor.core import TransactionProcessor

DEFAULT_URL = 'tcp://validator:4004'

LOGGER = logging.getLogger(__name__)

FAMILY_NAME = "grocerystore"
# TF Prefix is first 6 characters of SHA-512("grocerystore")

def _hash(data):
    '''Compute the SHA-512 hash and return the result as hex characters.'''
    return hashlib.sha512(data).hexdigest()

def _get_product_address(from_key, item):
    '''
    Return the address of a grocerystore object from the grocerystore TF.
    '''
    return _hash(FAMILY_NAME.encode('utf-8'))[0:6] + \
            _hash(item.encode('utf-8'))[0:6] + \
            _hash(from_key.encode('utf-8'))[0:58]


class GroceryStoreTransactionHandler(TransactionHandler):
    '''
    Transaction Processor class for the grocerystore Transaction Family.

    This TP communicates with the Validator using the accept/get/set functions.
    This implements functions to "add" or "remove" products in a grocery store.
    '''
    def __init__(self, namespace_prefix):
        '''Initialize the transaction handler class.

           This is setting the "grocerystore" TF namespace prefix.
        '''
        self._namespace_prefix = namespace_prefix

    @property
    def family_name(self):
        '''Return Transaction Family name string.'''
        return FAMILY_NAME

    @property
    def family_versions(self):
        '''Return Transaction Family version string.'''
        return ['1.0']

    @property
    def namespaces(self):
        '''Return Transaction Family namespace 6-character prefix.'''
        return [self._namespace_prefix]

    def apply(self, transaction, context):
        '''This implements the apply function for the TransactionHandler class.

           The apply function does most of the work for this class by
           processing a transaction for the grocerystore transaction family.
        '''

        # Get the payload and extract the grocerystore-specific information.
        # It has already been converted from Base64, but needs deserializing.
        # It was serialized with CSV: action, value
        header = transaction.header
        payload_list = transaction.payload.decode().split(",")
        item = payload_list[0]
        action = payload_list[1]
        amount = payload_list[2]

        # Get the signer's public key, sent in the header from the client.
        from_key = header.signer_public_key

        # Perform the action.
        LOGGER.info("Action = %s.", action)
        LOGGER.info("Amount = %s.", amount)
        if action == "procure":
            self._make_procure(context, amount, from_key, item)
        elif action == "sell":
            self._make_sell(context, amount, from_key, item)
        elif action == "clear":
            self._empty_product(context, amount, from_key, item)
        else:
            LOGGER.info("Unhandled action. Action should be procure or sell")

    @classmethod
    def _make_procure(cls, context, amount, from_key, item):
        '''Procure (add) "amount" to the product.'''
        product_address = _get_product_address(from_key, item)
        LOGGER.info('Got the key %s and the product address %s.',
                    from_key, product_address)
        state_entries = context.get_state([product_address])
        new_count = 0

        if state_entries == []:
            LOGGER.info('No previous product, adding new product to the grocery store %s.',
                        from_key)
            new_count = int(amount)
        else:
            try:
                count = int(state_entries[0].data)
            except:
                raise InternalError('Failed to load state data')
            new_count = int(amount) + int(count)

        state_data = str(new_count).encode('utf-8')
        addresses = context.set_state({product_address: state_data})

        if len(addresses) < 1:
            raise InternalError("State Error")

    @classmethod
    def _make_sell(cls, context, amount, from_key, item):
        '''Sell (subtract) "amount" from the product.'''
        product_address = _get_product_address(from_key, item)
        LOGGER.info('Got the key %s and the product address %s.',
                    from_key, product_address)

        state_entries = context.get_state([product_address])
        new_count = 0

        if state_entries == []:
            LOGGER.info('No product with the key %s.', from_key)
        else:
            try:
                count = int(state_entries[0].data)
            except:
                raise InternalError('Failed to load state data')
            if count < int(amount):
                raise InvalidTransaction('Not enough product in the store. '
                                         'The number should be <= %s.', count)
            else:
                new_count = count - int(amount)

        LOGGER.info('Selling %s out of %d.', amount, count)
        state_data = str(new_count).encode('utf-8')
        addresses = context.set_state(
            {_get_product_address(from_key, item): state_data})

        if len(addresses) < 1:
            raise InternalError("State Error")

    @classmethod
    def _empty_product(cls, context, amount, from_key, item):
        product_address = _get_product_address(from_key, item)
        LOGGER.info("fetched key %s and state address %s", from_key, product_address)
        state_entries = context.get_state([product_address])
        if state_entries == []:
            LOGGER.info('No product_name with the key %s.', from_key)
            return
        else:
            state_data = str(0).encode('utf-8')
            addresses = context.set_state(
                {product_address: state_data})

        if len(addresses) < 1:
            raise InternalError("State update Error")
        LOGGER.info("SET global state success")

def main():
    '''Entry-point function for the grocerystore Transaction Processor.'''
    try:
        # Setup logging for this class.
        logging.basicConfig()
        logging.getLogger().setLevel(logging.DEBUG)

        # Register the Transaction Handler and start it.
        processor = TransactionProcessor(url=DEFAULT_URL)
        sw_namespace = _hash(FAMILY_NAME.encode('utf-8'))[0:6]
        handler = GroceryStoreTransactionHandler(sw_namespace)
        processor.add_handler(handler)
        processor.start()
    except KeyboardInterrupt:
        pass
    except SystemExit as err:
        raise err
    except BaseException as err:
        traceback.print_exc(file=sys.stderr)
        sys.exit(1)

if __name__ == '__main__':
    main()
